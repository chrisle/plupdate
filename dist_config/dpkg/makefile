# MAKEFILE customized from source found here: https://github.com/benzap/eden/
#

# Makefile for generating dpkgs for the native-image compiled 'plupdate' executable
# Requires: leiningen
# Optional: plupdate

EMAIL := activeghost@live.com

GIT_ROOT_DIR := $(shell git rev-parse --show-toplevel)
PROJECT_ROOT_DIR := $(GIT_ROOT_DIR)
DIST_DIR := $(PROJECT_ROOT_DIR)/dist

OS := $(shell uname)
#ARCH := $(shell uname -i)
ARCH := $(shell dpkg --print-architecture)

PLUPDATE_VERSION := $(shell lein project-version)
PLUPDATE_EXE_NAME := plupdate-$(PLUPDATE_VERSION)

DEB_NAME := $(PLUPDATE_EXE_NAME)-$(ARCH)
DEB_VERSION := 1

DMAKE_DIR := $(DIST_DIR)/dpkg/$(DEB_NAME)
PROJ_PLUPDATE_EXE := $(PROJECT_ROOT_DIR)/bin/$(PLUPDATE_EXE_NAME)
DPKG_PLUPDATE_EXE := $(DMAKE_DIR)/usr/bin/$(PLUPDATE_EXE_NAME)


# default
all: dpkg


init:
	mkdir -p $(DMAKE_DIR)/DEBIAN
	touch $(DMAKE_DIR)/DEBIAN/control
	echo "Package: plupdate" >> $(DMAKE_DIR)/DEBIAN/control
	echo "Version: $(PLUPDATE_VERSION)-$(DEB_VERSION)" >> $(DMAKE_DIR)/DEBIAN/control
	echo "Section: interpreters" >> $(DMAKE_DIR)/DEBIAN/control
	echo "Priority: optional" >> $(DMAKE_DIR)/DEBIAN/control
	echo "Homepage: http://github.com/benzap/plupdate" >> $(DMAKE_DIR)/DEBIAN/control
	echo "Architecture: $(ARCH)" >> $(DMAKE_DIR)/DEBIAN/control
	echo "Maintainer: Benjamin Zaporzan <benzaporzan@gmail.com>" >> $(DMAKE_DIR)/DEBIAN/control
	echo "Description: Interpreter for the plupdate language" >> $(DMAKE_DIR)/DEBIAN/control

	cp prerm.template $(DMAKE_DIR)/DEBIAN/prerm
	chmod 775 $(DMAKE_DIR)/DEBIAN/prerm
	sed -i "s/PLUPDATE_EXE_NAME/$(PLUPDATE_EXE_NAME)/g" $(DMAKE_DIR)/DEBIAN/prerm

	cp postinst.template $(DMAKE_DIR)/DEBIAN/postinst
	chmod 775 $(DMAKE_DIR)/DEBIAN/postinst
	sed -i "s/PLUPDATE_EXE_NAME/$(PLUPDATE_EXE_NAME)/g" $(DMAKE_DIR)/DEBIAN/postinst

	cp $(PROJECT_ROOT_DIR)/LICENSE $(DMAKE_DIR)/DEBIAN/license

$(PROJ_PLUPDATE_EXE):
	echo "Building Native Image..."
	cd $(PROJECT_ROOT_DIR) && ./build-native.sh


$(DPKG_PLUPDATE_EXE): $(PROJ_PLUPDATE_EXE)
	mkdir -p $(DMAKE_DIR)/usr/bin
	cp $(PROJ_PLUPDATE_EXE) $(DPKG_PLUPDATE_EXE)
	chmod 755 $(DPKG_PLUPDATE_EXE)


prepare: $(DPKG_PLUPDATE_EXE)
	find $(DMAKE_DIR) -type d | xargs chmod 755


build:
	dpkg-deb --build $(DMAKE_DIR)


clean:
	rm -rf $(DMAKE_DIR)


dpkg: clean init prepare build