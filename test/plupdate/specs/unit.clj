(ns plupdate.specs.unit
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as stest]
            [plupdate.core :refer :all]
            [plupdate.playlist :refer :all]
            [plupdate.actions :refer :all]
            [taoensso.timbre :as timbre
             :refer [info]]))

;;
;; FIXTURES
;;
(defn one-time-setup []
  (println "one time setup"))

(defn one-time-teardown []
  (println "one time teardown"))

(defn once-fixture [f]
  (one-time-setup)
  (with-redefs []
    (f))
  (one-time-teardown))

(defn setup []
  (println "setup"))

(defn teardown []
  (println "teardown"))

(defn each-fixture [f]
  (setup)
  (f)
  (teardown))

(use-fixtures :once once-fixture)
(use-fixtures :each each-fixture)

(defn reduce-results
  [results]
  (reduce (fn [accumulator current]
            (and accumulator (get-in current [:clojure.spec.test.check/ret :pass?])))
          []
          results))

(defn log-results
  [results]
  (info :results (stest/summarize-results results)))

;; DB NAMESPACE
;; 
;; 
(deftest conform-spec
  (testing "conform generative tests"
    (let [results (stest/check `conform)]
      (log-results results)
      (is (= true (reduce-results results))))))

(deftest get-next-available-id-spec
  (testing "get-next-available-id generative tests"
    (let [results (stest/check `get-next-available-id)]
      (log-results results)
      (is (= true (reduce-results results))))))

(deftest create-spec
  (testing "create generative tests"
    (let [results (stest/check `create)]
      (log-results results)
      (is (= true (reduce-results results))))))

(deftest get-index-spec
  (testing "get-index generative tests"
    (let [results (stest/check `get-index)]
      (log-results results)
      (is (= true (reduce-results results))))))

(deftest dissocv-spec
  (testing "dissocv generative tests"
    (let [results (stest/check `dissocv)]
      (log-results results)
      (is (= true (reduce-results results))))))

(deftest remove-ids-spec
  (testing "remove-ids generative tests"
    (let [results (stest/check `remove-ids)]
      (log-results results)
      (is (= true (reduce-results results))))))

(deftest update-ids-spec
  (testing "update-ids generative tests"
    (let [results (stest/check `update-ids)]
      (log-results results)
      (is (= true (reduce-results results))))))

;; RULES ENGINE
;; 
;; 

(run-tests)