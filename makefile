# MAKEFILE customized from source found here: https://github.com/benzap/eden/
#


# Main makefile for building distributable packages of native plupdate
# executable
#
# For Debian-based systems:
#
# $ make dpgk
#
# For RPM-based systems:
#
# $ make rpm
#
# For a compressed archive:
#
# $ make tar
#
# To Install Locally from the repository:
#
# $ make install
#
# or include a custom PREFIX to install elsewhere:
#
# $ PREFIX=~/.bin make install
#
# Configuration:
#
# Requires: leiningen
#
# Requires: GraalVM with GRAAL_HOME environment variable set to the
# root of the graal folder (might work if you just have the command native-image on the path)
#
# Hardware Requirements: 64-bit Linux-based OS. Tested on Ubuntu 18.04
LEIN_CMD := lein


PREFIX := /usr/bin
PLUPDATE_VERSION := $(shell $(LEIN_CMD) project-version)
PLUPDATE_EXE_NAME := plupdate-$(PLUPDATE_VERSION)
PROJ_PLUPDATE_EXE := bin/$(PLUPDATE_EXE_NAME)


# default
all: clean build_native


# Generate plupdate native executable
build_native: $(PROJ_PLUPDATE_EXE)


# Generate deb Package for native executable
# Note: Tested on Ubuntu 18.04 / MINT 19.3
dpkg: $(PROJ_PLUPDATE_EXE)
	make -C dist_config/dpkg/


# Generate tar.gz Distribution for native executable
tar: $(PROJ_PLUPDATE_EXE)
	make -C dist_config/tar/


# Generate rpm Package for native executable
# Note: Tested on Fedora 28
rpm: $(PROJ_PLUPDATE_EXE)
	make -C dist_config/rpmpkg/


# Install Native Executable
# Note: Tested in linux
install: $(PROJ_PLUPDATE_EXE)
	cp $(PROJ_PLUPDATE_EXE) $(PREFIX)/$(PLUPDATE_EXE_NAME)
	chmod 755 $(PREFIX)/$(PLUPDATE_EXE_NAME)
	rm -f $(PREFIX)/plupdate
	ln -s $(PREFIX)/$(PLUPDATE_EXE_NAME) $(PREFIX)/plupdate


clean:
	rm -f $(PROJ_PLUPDATE_EXE)
	rm -rf dist
	rm -rf bin


distclean:
	rm -f $(PREFIX)/$(PLUPDATE_EXE_NAME)
	rm -f $(PREFIX)/plupdate


$(PROJ_PLUPDATE_EXE):
	sh ./build-native.sh
