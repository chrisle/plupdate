# Source Code in this native build script customized from: https://github.com/benzap/eden
#

#!/usr/bin/env bash

##
## Configuration
##

# Requires: Defined GRAAL_HOME to point at the root of your GRAALVM folder.
# Requires: Leiningen


# Set GRAAL VM to JAVA_HOME (uncomment if needed)
# export JAVA_HOME=$GRAAL_HOME

# Add GRAAL VM to the PATH, should include native-image
# export PATH=$GRAAL_HOME/bin:$PATH

# Lein Command Override
LEIN_CMD=lein

# Retrieve the current plupdate version
echo "Getting Project Version..."
PLUPDATE_VERSION=`$LEIN_CMD project-version`
echo "Project Version: " $PLUPDATE_VERSION
echo ""

echo "Generating Uberjar..."
$LEIN_CMD uberjar
echo ""

echo "Building Native Image..."
native-image -jar target/plupdate-$PLUPDATE_VERSION-standalone.jar \
             -H:Name="plupdate-${PLUPDATE_VERSION}" \
             --initialize-at-build-time \
             --no-fallback \
             --report-unsupported-elements-at-runtime \
             -H:EnableURLProtocols=http,https \
	     --enable-all-security-services \
	     -H:+ReportExceptionStackTraces \
             -H:+TraceClassInitialization \
             -H:ConfigurationFileDirectories=./meta 
echo ""

echo "Post Configuration..."
mkdir -p bin
chmod 744 plupdate-${PLUPDATE_VERSION}
mv plupdate-$PLUPDATE_VERSION ./bin/
rm -f ./bin/plupdate
ln -s ./bin/plupdate-$PLUPDATE_VERSION ./bin/plupdate
echo ""

echo "Built executable can be found at ./bin/plupdate-${PLUPDATE_VERSION}"

# Local testing
# native-image -jar target/plupdate-0.1.0-SNAPSHOT-standalone.jar \
#              -H:Name="plupdate-0.1.0-SNAPSHOT" \
#              --initialize-at-build-time \
#              --no-fallback \
#              --report-unsupported-elements-at-runtime \
# 	     --enable-all-security-services \
#              -H:EnableURLProtocols=http,https \
#              -H:+ReportExceptionStackTraces \
#              -H:+TraceClassInitialization \
#              -H:ConfigurationFileDirectories=./meta
#
# Note: This is failing with unsupported feature of java.io.FilePermission getting initialized. Not sure where from.