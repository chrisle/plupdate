(ns dev
  (:gen-class)
  (:require    [cheshire.core :as json]
               [clojure.edn :as edn]
               [clojure.java.io :as io]
               [clojure.tools.namespace.repl :as tn]))

;; REPL SETUP
;;
(defn repl-setup
  "Any code we need to run to get working in the REPL for this project add here.
 Remember that any extra code also increases the time it takes to load the REPL."
  []
        ;; clojure repl goodness
  (require '[clojure.edn :as edn])
  (require '[clojure.java.io :as io])
  (require '[clojure.java.javadoc :as jdoc])
  (require '[clojure.repl :refer :all])
  (require '[clojure.pprint :as pp])
  (require '[clojure.inspector :as insp])
  (require '[clojure.reflect :as reflect])
  (require '[clojure.tools.namespace.repl :refer [refresh]])

        ;; spec
  (require '[clojure.spec.alpha :as spec])
  (require '[clojure.spec.gen.alpha :as gen])
  (require '[clojure.spec.test.alpha :as stest])
  (require '[expound.alpha :as e])

          ;; project namespaces
  (require '[dev :as dev])
  (require '[plupdate.playlist])
  (require '[plupdate.actions]))

;; MOUNT 
;;
;; (defn start []
;;   (on-upndown :info log :before)
;;   (mount/start))

;; (defn stop []
;;   (mount/stop))

;; (defn refresh []
;;   (stop)
;;   (tn/refresh))

;; (defn refresh-all []
;;   (stop)
;;   (tn/refresh-all))

;; (defn reset []
;;   (stop)
;;   (tn/refresh :after 'dev/start))

(def sample-rules [[:remove :playlist [1 2 3 4]]
                   [:create :user ["bobby" "sally" "fred"]]
                   [:create :song [{:artist "Stone Temple Pilots"
                                    :title  "Crackerman"}
                                   {:artist "Alice In Chains"
                                    :title  "California is OK"}
                                   {:artist "Beatles"
                                    :title  "Yesterday"}]]
                   [:update :playlist [1] [{:song_ids [1 2 3 4]}]]
                   [:update :user [1] ["bobby minerva"]]
                   [:update :song [1 3 5 7] [{:artist "Bebe` Rexhal"}]]
                   [:read :song [1 2 3 4] [:artist]]])

(defn load-edn
  "Load edn from an io/reader source (filename or io/resource)."
  [source]
  (try
    (with-open [r (io/reader source)]
      (edn/read (java.io.PushbackReader. r)))

    (catch java.io.IOException e
      (printf "Couldn't open '%s': %s\n" source (.getMessage e)))
    (catch RuntimeException e
      (printf "Error parsing edn file '%s': %s\n" source (.getMessage e)))))

(defn load-json
  [filepath]
  (-> (clojure.java.io/resource filepath)
      slurp))

(defn json-resource->edn
  [filepath]
  (-> filepath
      load-json
      (json/parse-string true)))