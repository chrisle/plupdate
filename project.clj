(defproject plupdate "0.1.0-SNAPSHOT"
  :description "An amazing playlist batch processing tool"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [;; core language libraries
                 [org.clojure/clojure "1.10.2-alpha1"]     ;; required to pick up this fix: https://clojure.atlassian.net/browse/CLJ-1472 to compile to native.
                 [org.clojure/core.async "1.0.567"]

                 ;; COMMAND LINE HANDLING\
                 ;;
                 [cli-matic "0.3.11"]                       ;; https://clojars.org/cli-matic A command line arg parsing library
                 
                 ;; STATE HANDLING
                ;;  [mount "0.1.16"]                           ;; https://github.com/tolitius/mount Alternative to Component that doesn't force it's architecture on you.
                ;;  [tolitius/mount-up "0.1.2"]                ;; https://clojars.org/tolitius/mount-up

                 ;; DATABASE ACCESS
                 [org.clojure/java.jdbc "0.7.11"]

                 ;; JSON
                 [cheshire "5.9.0"]                         ;; https://github.com/dakrone/cheshire (fast, rich JSON parsing library)

                 ;; CONFIG HANDLING and ENV
                 [meta-merge "1.0.0"]                       ;;https://github.com/weavejester/meta-merge (rich map merge behavior)

                 ;; STORAGE

                 ;; LOGGING
                 [com.taoensso/timbre "4.10.0"]             ;; https://github.com/ptaoussanis/timbre native clojure logging framework

                 ;; SPEC
                 [expound "0.8.4"]                           ;; https://github.com/bhb/expound  ...for nice spec messages

                ;; uri validation (used in our specs)
                 [commons-validator/commons-validator "1.6"] ;; https://mvnrepository.com/artifact/commons-validator/commons-validator/1.6

                 ;; TESTING
                 [com.gfredericks/test.chuck "0.2.10"]       ;; https://github.com/gfredericks/test.chuck 
                 [org.clojure/test.check "0.10.0"]           ;; https://github.com/clojure/test.check
                 [org.clojure/data.generators "0.1.2"]       ;; https://github.com/clojure/data.generators
                 ]
  
  :min-lein-version "2.0.0"
  :resource-paths ["config", "resources"]

  :plugins [[lein-pprint "1.1.1"]
            [lein-cloverage "1.0.13"]]

  :aliases {"project-version" ["run" "-m" "plupdate.version/print-project-version"]}

  
  ;; If you use HTTP/2 or ALPN, use the java-agent to pull in the correct alpn-boot dependency
  ;:java-agents [[org.mortbay.jetty.alpn/jetty-alpn-agent "2.0.5"]]
  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.11"]]
                   :source-paths ["dev" "src"]
                   :test-paths ["test"]}
             :test [:dev {:dependencies [[org.clojure/test.check "0.10.0"]]}]    ;; https://github.com/clojure/test.check
             :ccov [:test {:source-paths ["src"]
                           :test-paths ["test"]
                           :cloverage {:test-ns-regex [#"^((?!(e2e|integration_tests)).)*$"]}}]
             :uberjar {:aot :all}}

  ;; Added for local REPL from terminal. For Calva (VS Code), you will need to add this to calva-repl startup to get "jack-in" to run them.
  :repl-options {:init (do
                         ;; run our dev setup
                         (require '[dev :as dev])
                         (dev/repl-setup))}
  
  :main ^{:skip-aot true} plupdate.core)