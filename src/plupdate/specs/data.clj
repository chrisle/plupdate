(ns plupdate.specs.data
  (:gen-class)
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [com.gfredericks.test.chuck.generators :as genx]
            [expound.alpha :as expound]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]))

(set! *warn-on-reflection* true)

(def id-regex #"^[0-9]*$")
(def file-path-regex #"[^\\0]+")
(def folder-path-regex #"^\/?([\d\w\.]+)(\/([\d\w\.]+))*\/?$")

(s/def ::significant-string (s/with-gen
                              (s/and string? #(not= % ""))
                              (fn [] (gen/such-that #(not= % "") (gen/string-alphanumeric)))))

;; CLI SPECS
;; 
(s/def ::file-path (s/with-gen
                     (s/and ::significant-string #(re-matches file-path-regex %))
                     (fn [] (gen/fmap #(str %) (genx/string-from-regex file-path-regex)))))

(s/def ::folder-path (s/with-gen
                       (s/and ::significant-string #(re-matches folder-path-regex %))
                       (fn [] (gen/fmap #(str %) (genx/string-from-regex folder-path-regex)))))

(s/def ::filepaths (s/coll-of ::file-path))

;; DATABASE SPECS
;; 
;; 

;; ints for now. uuids if we mean it.
(s/def ::numeric-string (s/with-gen
                          (s/and #(re-matches id-regex %))
                          (fn [] (gen/fmap #(str %) (s/gen (s/int-in 1 1000))))))
(s/def ::id ::numeric-string)
(s/def ::ids (s/with-gen 
              (s/coll-of ::id)
              (fn [] (gen/fmap #(when (seq %)(->> % (map str) vec)) (gen/set (s/gen (s/int-in 1 1000)))))))

(s/def ::song_ids ::ids)
(s/def ::user_id ::id)
;; tbd --add a generator that checks that all ids are unique
(s/def ::playlist (s/keys :req-un [::id ::user_id ::song_ids]))
(s/def ::playlists (s/coll-of ::playlist))

(s/def ::artist ::significant-string)
(s/def ::title ::significant-string)
;; tbd --add a generator that checks that all ids are unique
(s/def ::song (s/keys :req-un [::id ::artist ::title]))
(s/def ::songs (s/coll-of ::song))

(s/def ::name ::significant-string)
;; tbd --add a generator that checks that all ids are unique
(s/def ::user (s/keys :req-un [::id ::name]))
(s/def ::users (s/coll-of ::user))

(s/def ::db (s/keys :req-un [::playlists ::songs ::users]))

;; ACTION SPECS
;;
;; ACTIONS:
;; remove: [{command} {target type} [vector of ids]]
;; create: [{command} {target type} [vector of create data]]
;; update: [{command} {target type} [vector of ids] [vector of update data]]

(s/def ::song_ids ::ids)
(s/def ::user_id ::id)

;; Supported operations:
;; Create - New Playlist, New Song, New User 
;; Update - Add new songs to a playlist, Update a user name, 
;; not supporting transfer of playlists. No one asked for it :)
(s/def ::playlist-create-data (s/keys :req-un [::user_id]
                                      :opt-un [::song_ids]))
(s/def ::song-create-data (s/keys :req-un [::artist ::title]))
(s/def ::user-create-data (s/keys :req-un [::name]))
(s/def ::playlist-creates (s/coll-of ::playlist-create-data))
(s/def ::song-creates (s/coll-of ::song-create-data))
(s/def ::user-creates (s/coll-of ::user-create-data))

(s/def ::playlist-update-data (s/keys :req-un [::song_ids]))
(s/def ::user-update-data (s/keys :req-un [::name]))
(s/def ::song-update-data (s/keys :req-un [(or ::artist ::title)]))

(def command? #{:create :remove :update})
(def type? #{:playlist :song :user})
(def container? #{:playlists :songs :users})

(s/def ::commands command?)
(s/def ::types type?)
(s/def ::container container?)

(def create-playlist (s/cat :command :create :type :playlist :data ::playlist-creates))
(def create-song (s/cat :command :create :type :song :data ::song-creates))
(def create-user (s/cat :command :create :type :user :data ::user-creates))

(defmulti create-spec? second)
(defmethod create-spec? :playlist [_] create-playlist)
(defmethod create-spec? :song [_] create-song)
(defmethod create-spec? :user [_] create-user)

(def update-playlist (s/cat :command ::update :type :playlist :data ::playlist-update-data))
(def update-song (s/cat :command ::update :type :song :data ::song-update-data))
(def update-user (s/cat :command ::update :type :user :data ::user-update-data))

(defmulti update-spec? :tag)
(defmethod update-spec? :playlist [_] update-playlist)
(defmethod update-spec? :song [_] update-song)
(defmethod update-spec? :user [_] update-user)

;; SPECS FOR GENERATION of Actions
;; 
;; need these since generated specs need to obey some rules.
(def create-type->specs
  {:playlist ::playlist
   :song ::song
   :user ::user})

(def update-type->specs
  {:playlist ::playlist-update
   :song ::song-update
   :user ::user-update})

(def collection-type->specs
  {:playlists ::playlists
   :songs ::songs
   :users ::users})

(s/def ::containers (s/or :playlists ::playlists :songs ::songs :users ::users))
(s/def ::create-data-types (s/or :playlist ::playlist-creates :songs ::song-creates :users ::user-creates))
(s/def ::update-data-types (s/or :playlist ::playlist-update-data :song ::song-update-data :user ::user-update-data))

(def create-action-pattern  (s/cat :command #{:create} :type ::types :data ::create-data-types))
(def remove-action-pattern  (s/cat :command #{:remove} :type ::types :ids ::ids))
(def update-action-pattern  (s/cat :command #{:update} :type ::types :ids ::ids :data ::update-data-types))

;; ACTION DEFS
;; 
;; 
(s/def ::create-action
  (s/with-gen  #(s/multi-spec create-spec? %)
    (fn [] (gen/such-that #(let [genv (vec %)
                                 type (second genv)
                                 data (last genv)]
                             (trace :sample {:type type :data % :list genv})
                             (s/valid? (type create-type->specs) data))
                          (s/gen create-action-pattern)))))

(s/def ::remove-action
  (s/with-gen (s/and vector? remove-action-pattern)
    #(gen/fmap vec (s/gen remove-action-pattern))))

(s/def ::update-action
  (s/with-gen #(s/multi-spec update-spec? %)
    (fn [] (gen/such-that #(let [genv (vec %)
                                 type (second genv)
                                 data (last genv)]
                             (trace :sample {:type type :data (count data) :vector genv})
                             (s/valid? (type update-type->specs) data))
                          (s/gen update-action-pattern)))))

(defmulti action-spec? first)
(defmethod action-spec? :create [_] ::create-action)
(defmethod action-spec? :remove [_] ::remove-action)
(defmethod action-spec? :update [_] ::update-action)

(s/def ::action (s/with-gen #(s/multi-spec action-spec? %)
                  (fn [] (s/gen (rand-nth [::create-action ::remove-action ::update-action])))))

(s/def ::actions (s/coll-of ::action))

(defn valid? [fn-key spec data]
  (let [result (s/valid? spec data)]
    (trace :validity_check {:function fn-key :spec spec :result result})
    (when (false? result)
      (error :spec-error {:explain (expound/expound-str spec result) :data data}))
    result))