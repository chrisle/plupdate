(ns plupdate.actions
  (:gen-class)
  (:require [clojure.spec.alpha :as s]
            [plupdate.common :as c]
            [plupdate.specs.data :as plspecs]
            [plupdate.playlist :as db]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]))

(set! *warn-on-reflection* true)

;; EXERCISE: 
;; 
;; Ingest mixtape.json and a changes file. The changes file should include multiple
;; changes in a single file:
;; ❏ Add a new playlist; the playlist should contain at least one song.
;; ❏ Remove a playlist.
;; ❏ Add an existing song to an existing playlist.

;; JSON format defined in the resource file.
;; Users / Playlists / Songs
;; users create playlists, playlists contain songs
;; 
;; 
;; Support CUD operations across these entities.

;; Principal is "return the things we did"
;; 

;; remove: [{command} {target type} [vector of ids]]
;; create: [{command} {target type} [vector of type data]]
;; update: [{command} {target type} [vector of ids] [vector of data to merge in to each id (update or create)]]
;; read: [{command} [vector of ids] opt: [attributes to read out] ]

(def type->container
  {:playlist :playlists
   :song :songs
   :user :users})

(s/fdef apply-create
  :args (s/cat :db ::plspecs/db
               :action ::plspecs/create-action)
  :ret ::plspecs/db)

(defn apply-create
  [db action]
  {:pre [(c/pre-cond? :apply-create ::plspecs/db db)
         (c/pre-cond? :apply-create ::plspecs/create-action action)]
   :post [(c/post-cond? :apply-create ::plspecs/db %)]}
  (let [container ((get action 1) type->container)
        coll (get action 2)]
    (debug :params {:data coll})
    (db/create db container coll)))

(s/fdef apply-remove
  :args (s/cat :db ::plspecs/db
               :action ::plspecs/remove-action)
  :ret ::plspecs/db)

(defn apply-remove
  [db action]
  {:pre [(c/pre-cond? :apply-remove ::plspecs/db db)
         (c/pre-cond? :apply-remove ::plspecs/remove-action action)]
   :post [(c/post-cond? :apply-remove ::plspecs/db %)]}
  (trace :params {:action action})
  (let [container ((get action 1) type->container)
        coll (get action 2)]
    (db/remove-ids db container coll)))

(s/fdef apply-update
  :args (s/cat :db ::plspecs/db
               :action ::plspecs/create-action)
  :ret ::plspecs/db)

(defn apply-update
  [db action]
  {:pre [(c/pre-cond? :apply-update ::plspecs/db db)
         (c/pre-cond? :apply-update ::plspecs/update-action action)]
   :post [(c/post-cond? :apply-update ::plspecs/db %)]}
  (let [container ((get action 1) type->container)
        ids (get action 2)
        data (get action 3)]
    (info :execute-update {:id ids :data data})
        (db/update-ids db container ids data)))

(s/fdef apply-action
  :args (s/cat :db ::plspecs/db :action ::plspecs/action)
  :ret ::plspecs/db)
(defn apply-action
  [db action]
  {:pre [(c/pre-cond? :apply-action ::plspecs/db db)
         (c/pre-cond? :apply-action ::plspecs/action action)]
   :post [(c/post-cond? :apply-action ::plspecs/db %)]}
  (info :executing {:action action})
  (let [action-type (get action 0)]
    (condp = action-type
      :create (apply-create db action)
      :remove (apply-remove db action)
      :update (apply-update db action)

      ;; default, return the db unchanged.
      (do (warn :unsupported-action {:action action})
          db))))

(defn apply-actions
  [actions db]
  {:pre [(c/pre-cond? :apply-actions ::plspecs/db db)
         (c/pre-cond? :apply-actions ::plspecs/actions actions)]
   :post [(c/post-cond? :apply-actions ::plspecs/db %)]}
  (trace :params {:action-count (count actions) :db-size (count db)})
  (reduce (fn [accum rule] (apply-action accum rule)) db actions))
