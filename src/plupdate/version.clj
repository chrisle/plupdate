(ns plupdate.version
    (:gen-class))

(set! *warn-on-reflection* true)

(defn get-project-version []
  (java.lang.System/getProperty "plupdate.version"))


(defn print-project-version []
  (println (get-project-version)))