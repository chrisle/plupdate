(ns plupdate.common
  (:gen-class)
  (:require [plupdate.specs.data :as plspecs]))

(defn pre-cond? [fnkey spec data]
  (plspecs/valid? [fnkey :pre] spec data))

(defn post-cond? [fnkey spec data]
  (plspecs/valid? [fnkey :post] spec data))