(ns plupdate.core
  (:gen-class)
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.walk :as walk]
            [cli-matic.core :refer [run-cmd]]
            [plupdate.actions :as rengine]
            [plupdate.specs.data :as specs]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]))

(defn process-file
  [file actions output-path]
  (spit output-path (-> actions
                        (rengine/apply-actions file)
                        json/generate-string)))

(defn run
  [{:keys [actions files output-path]}]
  (let [files (walk/keywordize-keys files)]
    (doall (pmap #(process-file % actions output-path) [files])))
  true)

(def CONFIGURATION
  {:app         {:command     "plupdate"
                 :description "A command-line music playlist database munger"
                 :version     "0.0.1"}

   :global-opts []

   :commands    [{:command     "process" :short "p"
                  :description "Applies a set of actions to a playlist"
                  :opts        [{:option "actions" :short 0 :type :ednfile :spec ::specs/actions :default :present}
                                {:option "files" :short 1 :type :jsonfile :default :present}
                                {:option "output-path" :short "o" :type :string :spec ::specs/folder-path :default "./output/playlist.json"}]
                  :runs        run}]})

(defn -main
  "This is our entry point.
  Just pass parameters and configuration.
  Commands (functions) will be invoked as appropriate."
  [& args]
  (run-cmd args CONFIGURATION))
