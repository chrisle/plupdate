(ns plupdate.playlist
  (:gen-class)
  (:require [cheshire.core :as json]
            [clojure.data :refer [diff]]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [expound.alpha :as expound]
            [plupdate.common :as c]
            [plupdate.specs.data :as plspecs]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]])
  (:import javax.naming.directory.SchemaViolationException))

(set! *warn-on-reflection* true)

(defn load-from-file
  [path]
  (info :config-path path)
  (-> (io/resource path)
      slurp
      (json/parse-string true)))

(s/fdef conform
  :args (s/cat :unknown-data ::plspecs/db)
  :ret ::plspecs/db)
(defn conform
  [unknown-data]
  (let [data (s/conform ::plspecs/db unknown-data)]
    (if (= data ::s/invalid)
      (let [error-msg (expound/expound-str ::plspecs/db data)]
        (error :spec-error error-msg)
        (throw (SchemaViolationException. error-msg)))
      data)))

(defn reducer
  [accum next]
  (if (>= (- next accum) 2) (reduced (inc accum)) next))

(s/def ::get-next-available-id-args
  (s/with-gen
    (s/cat :coll ::plspecs/containers)
    #(gen/not-empty (s/gen ::plspecs/containers))))

(s/fdef get-next-available-id
  :args (s/cat :coll ::plspecs/containers)
  :ret ::plspecs/id)

(defn get-next-available-id
  [coll]
  {:pre [(c/pre-cond? :get-next-available-id ::plspecs/containers coll)]
   :post [(c/post-cond? :get-next-available-id ::plspecs/id %)]}
  (let [ids (or (seq (map #(java.lang.Integer/parseInt (:id %)) coll)) '(0))
        next-id (reduce reducer 0 ids)
        last-id (last ids)]
    ;; We default to 0 above if the id list is empty so we get 1 after the logic below
    (str (if (= next-id last-id)
              (inc last-id)
              next-id))))

(defn compare-maps
  [a b]
  {:pre [(map? a)
         (map? b)]}
  (let [diffs (diff a b)
        a-diff (first diffs)
        b-diff (second diffs)]
    (cond
      (and (not (nil? a-diff)) (nil? b-diff)) -1
      (and (nil? a-diff) (nil? b-diff)) 0
      (and (nil? a-diff) (not (nil? b-diff))) 1
      (and (not (nil? a-diff)) (not (nil? b-diff))) 2)))

(def create-arg-gen
  (gen/fmap
   (fn [[db container]]
     (let [data  (-> container
                     plspecs/collection-type->specs
                     s/gen
                     gen/generate)]
       (trace :generated-params [:db db :container container :data data])
       [db container data]))
   (s/gen
    (s/cat :db ::plspecs/db :container ::plspecs/container))))

(s/def ::create-id-args
  (s/with-gen
    (s/cat :db ::plspecs/db :container ::plspecs/container :data ::plspecs/create-data-types)
    (constantly create-arg-gen)))

(s/fdef create
  :args ::create-id-args ;;(s/cat :db ::plspecs/db :container ::plspecs/container :data ::plspecs/create-data-types)
  :ret ::plspecs/db
  :fn (fn [{:keys [args ret]}]
        (let [{:keys [db container data]} args
              before-count (count (container db))
              after-count (count (container ret))
              data-count (count (second data))
              expected (+ before-count data-count)
              retval (= after-count expected)]
          (info :create-content-test {:retval retval :before before-count :data-count data-count :actual after-count :expected expected})
          retval)))

(defn create
  [db container data]
  {:pre [(c/pre-cond? :create ::plspecs/db db)
         (c/pre-cond? :create ::plspecs/container container)
         (c/pre-cond? :create ::plspecs/create-data-types data)]
   :post [(c/post-cond? :create ::plspecs/db %)]}
  (loop [create-data-coll data
         db db]
    (if (seq create-data-coll)
      (let [id (get-next-available-id (container db))]
        (trace :loop-params {:id id :first (first create-data-coll) :db-size (count db) :container-exists? (contains? db container)})
        (recur (vec (rest create-data-coll))
               (->> {:id id}
                    (conj (first create-data-coll))
                    (conj (container db))
                    (assoc-in db [container]))))
      (do (info :created {container (count data) :total (count (container db))})
          db))))

(def get-index-arg-gen
  (gen/fmap
   (fn [coll]
     (let [id (rand-nth (vec (map #(:id %) coll)))]
       (info {:id id :db-section coll})
       [coll id]))
   (gen/not-empty (s/gen ::plspecs/containers))))

(s/def ::get-index-args
  (s/with-gen
    (s/cat :coll ::plspecs/containers :id ::plspecs/id)
    (constantly get-index-arg-gen)))

(s/fdef get-index
  :args ::get-index-args
  :ret int?)

(defn get-index
  [coll id]
  {:pre [(c/pre-cond? :get-index ::plspecs/containers coll)
         (c/pre-cond? :get-index ::plspecs/id id)]
   :post [(int? %)]}
  (let [idx (keep-indexed #(when (= (:id %2) id) %1) coll)]
    (trace :index {:id id :idx idx})
    (if (seq idx)
      (first idx)
      (throw (java.lang.ArrayIndexOutOfBoundsException. (str "Action specified an index that doesn't exist, id: " id))))))

(def dissocv-arg-gen
  (gen/fmap
   (fn [coll]
     (let [id (rand-nth (vec (map #(:id %) coll)))
           idx (get-index coll id)]
       (info {:id id :db-section coll})
       [coll idx]))
   (gen/not-empty (s/gen ::plspecs/containers))))

(s/def ::dissocv-args
  (s/with-gen
    (s/cat :v ::plspecs/containers :idx int?)
    (constantly dissocv-arg-gen)))

(s/fdef dissocv
  :args ::dissocv-args
  :ret vector?)

(defn dissocv
  [v idx]
  (vec (concat (subvec v 0 idx) (subvec v (inc idx)))))

(def remove-arg-gen
  (gen/fmap
   (fn [arg]
     (let [[db container] arg
           ids (vec (map #(:id %) (container db)))]
       (trace {:container container :ids ids :db-section (container db)})
       [db container ids]))
   (s/gen
    (s/and
     (s/cat :db ::plspecs/db :container ::plspecs/container)
     (fn [{:keys [db container]}]
       (trace {:target-keys (count (container db))})
       (> (count (container db)) 0))))))

(s/def ::remove-id-args
  (s/with-gen
    (s/cat :db ::plspecs/db :container ::plspecs/container :ids ::plspecs/ids)
    (constantly remove-arg-gen)))

(s/fdef remove-ids
  :args ::remove-id-args ;;(s/cat :db ::plspecs/db :container ::plspecs/container :id ::plspecs/id)
  :ret ::plspecs/db
  :fn (fn [{:keys [args ret]}]
        (let [{:keys [db container ids]} args
              before-count (count (container db))
              after-count (count (container ret))
              id-count (count (second ids))
              expected (- before-count id-count)
              retval (if (< before-count id-count) (= after-count before-count) (= after-count expected))]
          (info :remove-content-test {:retval retval :before before-count :id-count id-count :actual after-count :expected expected})
          retval)))

(defn remove-ids
  [db container ids]
  {:pre [(c/pre-cond? :remove-ids ::plspecs/db db)
         (c/pre-cond? :remove-ids ::plspecs/container container)
         (c/pre-cond? :remove-ids ::plspecs/ids ids)]}
  (loop [db db
         ids-rest ids]
    (trace ids-rest)
    (if (seq ids-rest)
      (let [db (->> ids-rest
                    first
                    (get-index (container db))
                    (dissocv (container db))
                    (assoc-in db [container]))]
        (recur db (vec (rest ids-rest))))
      (do (info :removed {container (count ids) :remaining (count (container db))})
          db))))

(def update-arg-gen
  (gen/fmap
   (fn [arg]
     (let [[db container data] arg
           ids (vec (map #(:id %) (container db)))]
       (info {:container container :ids ids :data data :db-section (container db)})
       [db container ids data]))
   (s/gen
    (s/and
     (s/cat :db ::plspecs/db :container ::plspecs/container :data ::plspecs/update-data-types)
     (fn [{:keys [db container]}]
       (info (count (container db)))
       (> (count (container db)) 0))))))

(s/def ::update-id-args
  (s/with-gen
    (s/cat :db ::plspecs/db :container ::plspecs/container :ids ::plspecs/ids :data ::plspecs/update-data-types)
    (constantly update-arg-gen)))

(s/fdef update-ids
  :args ::update-id-args
  :ret ::plspecs/db)
(defn update-ids
  [db container ids data]
  {:pre [(c/pre-cond? :update-ids ::plspecs/db db)
         (c/pre-cond? :update-ids ::plspecs/container container)
         (c/pre-cond? :update-ids ::plspecs/update-data-types data)
         (c/pre-cond? :update-ids ::plspecs/ids ids)]
   :post [(c/post-cond? :update-ids ::plspecs/db %)]}
  (info :updating ids)
  (loop [db db
         id-set ids]
    (if (seq id-set)
      (let [idx (get-index (container db) (first id-set))
            updated-db (assoc-in db [container idx]
                                 (-> (container db)
                                     (get idx)
                                     (merge data)))]
        (debug :updated {:idset id-set :idx idx})
        (recur updated-db (rest id-set)))
      (do (info :updated {container (count ids)})
          db))))